export enum SelectionMode {
    Single = 'single-selection',
    Multiple = 'multiple-selection',
    Expander = 'expander-selection',
    None = 'none-selection',
};

export default SelectionMode;
