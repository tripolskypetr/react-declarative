import * as React from 'react';
import { useState } from 'react';
import { makeStyles } from '../../../../../styles';

import TableRow from '@mui/material/TableRow';

import CheckboxBodyCell from './common/CheckboxBodyCell';
import CommonBodyCell from './common/CommonBodyCell';

import IRowData from '../../../../../model/IRowData';
import IAnything from '../../../../../model/IAnything';

import useProps from '../.../../../../hooks/useProps';

interface IDesktopBodyRowProps<RowData extends IRowData = IAnything> {
    row: RowData;
    fullWidth: number;
}

const useStyles = makeStyles({
    noBottomBorder: {
        '& > *': {
            borderBottom: '0 !important',
        },
    },
});

export const DesktopBodyRow = <RowData extends IRowData = IAnything>({
    row,
    fullWidth,
}: IDesktopBodyRowProps<RowData>) => {

    const [menuOpened, setMenuOpened] = useState(false);
    const classes = useStyles();

    const props = useProps<RowData>();

    const {
        onRowClick,
        onRowAction,
        columns = [],
    } = props;

    const handleClick = () => {
        if (!menuOpened) {
            onRowClick && onRowClick(row);
        }
    };

    const handleMenuToggle = (opened: boolean) => {
        setMenuOpened(opened);
    };

    const handleAction = (action: string) => {
        onRowAction && onRowAction(row, action);
    };

    return (
        <TableRow
            className={classes.noBottomBorder}
            onClick={handleClick}
        >
            <CheckboxBodyCell<RowData> row={row} />
            {columns.map((column, idx) => (
                <CommonBodyCell<RowData>
                    column={column}
                    row={row}
                    key={idx}
                    idx={idx}
                    fullWidth={fullWidth}
                    onAction={handleAction}
                    onMenuToggle={handleMenuToggle}
                />
            ))}
        </TableRow>
    );
};

export default DesktopBodyRow;
